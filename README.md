# The Case of Blood Pressures

In early 2024 I occasionally experieneced a very acute symptom that my general practitioner (GP) failed to give an explanation for and refused to refer me to a specialist for. It felt like the highest altitude that my blood could be transported to was gradually going down; when it fell below my cheeks my lips went numb, and when it reached my stomach I threw up, unless the drop was slowly undone by lying down. At that moment this unexplained symptom occurred, I was unable to travel to the GP, and when I did, every regular check-up turned out fine. Meanwhile I had been suffering from abnormally low blood pressures. 

To figure out the connection between low blood pressures and the unexplained symptom, and also to convince my GP that this is not just a mild condition I need to live with (e.g pharyngitis), I measured my blood pressures when the symptom occurred or every few hours elsewhen. Over time I also started to document other symptoms I experienced and some plausible factors. Eventually I managed to persuade my GP to prescribe me a blood test.

## Research Questions
1. Have low blood pressures contributed to the unexplained symptom?
2. What other symptoms are significantly correlated with the unexplained symptom?
3. Does any of what are recommended by academic research (e.g coffee, licorice e.t.c) effectively improve blood pressures, at least to me?

## Dataset

654 rows (meausurements) $\times$ 13 columns (variables)

The dataset is manually curated and therefore perfectly neat.

### Time
February 14, 2024 - March 31, 2024

(I did not measure blood pressures on March 29, 2024 due to personal reasons)

### Variables

- **DATE**: format MM/DD/YYYY, e.g 03/29/2024
- **TIME**:	 format hh/mm, e.g 20:08
- **POSITION**: _left arm_ or _right wrist_, measured by 2 different types of machine
- **SYSTOLIC PRESSURE** (mmHg):	normal values for healthy East Asian female are [85-120](https://www.chinagp.net/CN/10.12114/j.issn.1007-9572.2018.00.157#:~:text=%E7%BB%93%E8%AE%BA%E4%B8%8D%E5%90%8C%E6%80%A7%E5%88%AB%E4%BA%BA%E7%BE%A4%E6%AD%A3%E5%B8%B8,Hg%E5%8F%AF%E8%83%BD%E5%AD%98%E5%9C%A8%E9%AB%98%E8%A1%80%E5%8E%8B%E3%80%82)
- **DIASTOLIC PRESSURE** (mmHg): normal values for healthy East Asian female are [55-80](https://www.chinagp.net/CN/10.12114/j.issn.1007-9572.2018.00.157#:~:text=%E7%BB%93%E8%AE%BA%E4%B8%8D%E5%90%8C%E6%80%A7%E5%88%AB%E4%BA%BA%E7%BE%A4%E6%AD%A3%E5%B8%B8,Hg%E5%8F%AF%E8%83%BD%E5%AD%98%E5%9C%A8%E9%AB%98%E8%A1%80%E5%8E%8B%E3%80%82)	
- **HEART RATE** / minute: normal values for healthy East Asian female are [55-95](https://www.alljournals.cn/view_abstract.aspx?pcid=A9DB1C13C87CE289EA38239A9433C9DC&cid=AD36F34DDC3BA7B0&jid=0EE04D4A880DEF3AD4A91E2696CB133F&aid=CDF8D098C5099B32&yid=14E7EF987E4155E6&vid=&iid=&sid=&eid=&from_absract=1)
- **DIARRHEA**:	_yes_ or _no_, if diarrhea occurred during the last 1 hour
- **UNEXPLAINED SYMPTOM**:	_yes_ or _no_, if the unexplained symptom occurred during the last 1 hour
- **DIZZINESS**: _yes_ or _no_, if dizziness occurred during the last 1 hour	
- **ALLERGY**:	_yes_ or _no_, if an allergy occurred during the last 1 hour
- **LICORICE**: _yes_ or _no_, if I consumed licorice tea during the last 1 hour
- **COFFEE**: _yes_ or _no_, if I consumed coffee during the last 1 hour	
- **GINGER**: _yes_ or _no_, if I consumed ginger water during the last 1 hour

### Measurement of Blood Pressures

Because I had to lie down upon the occurrence of the unexplained symptom, and I started with a small machine for the wrists, blood pressures were first measured on my right wrist, positioned horizontally on the same level as the body, for the convience of reaching the machine. A few days later a bigger, professional machine was added to the project, which was used on my left upper arm [in the recommended way](https://www.cdc.gov/bloodpressure/measure.htm#:~:text=First%2C%20a%20health%20care%20professional,will%20measure%20your%20blood%20pressure.) except the lie-down position. To some extent the position might contribute to lower measurements of blood pressures, but should not affect heart rate perceivably.

![](https://image.shutterstock.com/image-photo/shavasana-man-woman-wearing-sportswear-260nw-2226178293.jpg)

Ideally I should have measured my blood pressures 3 times every morning, every afternoon, and every bedtime with 5 minutes apart, then calculated the average. However, I did not manage that every time.

## Data Preparation

There are no errors or missing data in the dataset.

For every 6 rows measured within 1 hour, reduce them into 2 rows (each on one machine) by computing the average blood pressures & heart rate and taking the repetitive binary factors. I did find the numbers might change more rapidly in the morning and the bedtime, so averaging might be a good idea on its own.

![A snapshot of the dataset](dataset_peek.png)

## Interactive Visualization
R + Plotly

quadratic interpolation

with adjustable time frame

highlight data points of the symptom


## Periodity

## Anomaly Detection

## Hypothesis Tests

## Code

R code files are named after the corresponding sections of this project.

To clone this repository to a local device, use the following Git command:
```
cd existing_repo
git clone
```

To contribute to this repository by pushing new files/changes, use the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Paw_in_Data/the-case-of-blood-pressures.git
git branch -M main
git push -uf origin main
```

## Thoughts
Visualization goes a long way. An interactive one goes even further.

In contrast to high blood pressures, medical professionals don't take low blood pressures seriously in general.

Data collection would have been automated if I used a wearable monitor which could be programmed to take measurements at certain times and to upload data via bluetooth or narrowband. In that case some data wrangling should be necessary. Unfortunately [there hasn't been an equipment accurate and handy enough in the market](https://www.consumerreports.org/health/blood-pressure-monitors/measuring-blood-pressure-with-a-wearable-device-a9251907587/) yet; for example, an inflatable cuff will get in the way when I code on a laptop. I couldn't help but picture the future of digital health while waiting for numbers to pop out on bed.

## Contact
Feel free to [email](mailto:pawindata@zohomail.eu) me or visit my website if you have any suggestions or are simply curious!

![my signiture](https://media.istockphoto.com/id/1204633161/vector/footprints-of-a-white-bear-footprints-of-a-five-finger-beast.jpg?s=612x612&w=0&k=20&c=oi0_42u9ok5x4pxcxJeVswFPQbxHA--Bn87UuSddvi8=)
